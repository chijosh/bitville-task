import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home'
import Contact from '@/views/Contact'
import Project from '@/views/Projects'
import Resume from '@/views/Resume'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/resume',
      name: 'Resume',
      component: Resume
    },
    {
      path: '/projects',
      name: 'Project',
      component: Project
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    }
  ]
})

export default router
